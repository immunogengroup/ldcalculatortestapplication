package nl.umcg.ldcalculatortestingapplication;

import java.util.ArrayList;
import java.util.List;

public class LdVariantPair {

    private String variant1;
    private String variant2;


    public LdVariantPair(String variant1, String variant2) {
        this.variant1 = variant1;
        this.variant2 = variant2;
    }


    public List<String> getVariants() {
        List<String> out = new ArrayList<>();
        out.add(variant1);
        out.add(variant2);
        return out;

    }

    public String getVariant1() {
        return variant1;
    }

    public String getVariant2() {
        return variant2;
    }
}
