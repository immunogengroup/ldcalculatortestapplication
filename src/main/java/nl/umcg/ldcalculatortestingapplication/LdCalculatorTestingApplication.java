package nl.umcg.ldcalculatortestingapplication;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;
import org.molgenis.genotype.RandomAccessGenotypeData;
import org.molgenis.genotype.RandomAccessGenotypeDataReaderFormats;
import org.molgenis.genotype.util.LdCalculatorException;
import org.molgenis.genotype.variant.GeneticVariant;
import org.molgenis.genotype.variantFilter.VariantFilter;
import org.molgenis.genotype.variantFilter.VariantIdIncludeFilter;

import java.io.*;
import java.util.*;

public class LdCalculatorTestingApplication {

    private static final Logger LOGGER = Logger.getLogger(LdCalculatorTestingApplication.class);

    public static void main(String[] args) {
        try {
            CommandLineParser parser = new DefaultParser();
            CommandLine cmd = parser.parse(LdCalculatorTestingApplicationParameters.getOptions(), args);

            List<LdVariantPair> ldPairs = readFileAsMap(cmd.getOptionValue('i').trim());
            Set<String> allVariants = new HashSet<>();

            for (LdVariantPair p : ldPairs) {
                allVariants.addAll(p.getVariants());
            }

            RandomAccessGenotypeData genotypeData = readGenotypeData(cmd.getOptionValue('g').trim(),
                    cmd.getOptionValue('t').trim(),
                    new VariantIdIncludeFilter(allVariants));


            Map<String, GeneticVariant> variants = genotypeData.getVariantIdMap();

            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(cmd.getOptionValue('o').trim())));

            for (LdVariantPair curPair : ldPairs) {
                GeneticVariant variantA = variants.get(curPair.getVariant1());
                GeneticVariant variantB = variants.get(curPair.getVariant2());

                bw.append(variantA.getPrimaryVariantId());
                bw.append("\t");
                bw.append(variantB.getPrimaryVariantId());
                bw.append("\t");
                bw.append(Double.toString(variantA.calculateLd(variantB).getR2()));
                bw.newLine();
            }

            bw.flush();
            bw.close();


        } catch (ParseException e) {
            LdCalculatorTestingApplicationParameters.printHelp();
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LdCalculatorException e) {
            e.printStackTrace();
        }


    }

    public static RandomAccessGenotypeData readGenotypeData(String path, String fmt) throws IOException {
        return readGenotypeData(path, fmt, null);
    }

    public static RandomAccessGenotypeData readGenotypeData(String path, String fmt, VariantFilter filter) throws IOException {

        RandomAccessGenotypeDataReaderFormats format = RandomAccessGenotypeDataReaderFormats.valueOfSmart(fmt);
        RandomAccessGenotypeData gt = null;

        switch (format) {
            case GEN:
                throw new UnsupportedOperationException("Not yet implemented");
            case GEN_FOLDER:
                throw new UnsupportedOperationException("Not yet implemented");
            case PED_MAP:
                throw new UnsupportedOperationException("Not yet implemented");
            case PLINK_BED:
                if (filter == null) {
                    gt =  RandomAccessGenotypeDataReaderFormats.PLINK_BED.createGenotypeData(path);
                } else {
                    gt =  RandomAccessGenotypeDataReaderFormats.PLINK_BED.createFilteredGenotypeData(path, 1000, filter, null);
                }
                break;
            case SHAPEIT2:
                throw new UnsupportedOperationException("Not yet implemented");
            case TRITYPER:
                throw new UnsupportedOperationException("Not yet implemented");
            case VCF:
                if (filter == null) {
                    gt = RandomAccessGenotypeDataReaderFormats.VCF.createGenotypeData(path);
                } else {
                    gt =  RandomAccessGenotypeDataReaderFormats.VCF.createFilteredGenotypeData(path, 1000, filter, null);
                }
                break;
            case VCF_FOLDER:
                throw new UnsupportedOperationException("Not yet implemented");
        }

        return gt;
    }


    public static List<LdVariantPair> readFileAsMap(String file) throws IOException {
        BufferedReader reader = new BufferedReader(new java.io.FileReader(file));
       List<LdVariantPair> content = new ArrayList<>();

        String line;
        while ((line = reader.readLine()) != null) {
            content.add(new LdVariantPair(line.split("\\t")[0], line.split("\\t")[1]));
        }
        return(content);
    }


}
