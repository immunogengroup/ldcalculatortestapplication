package nl.umcg.ldcalculatortestingapplication;

import org.apache.commons.cli.*;


public class LdCalculatorTestingApplicationParameters {

    private static final Options OPTIONS;

    static {
        OPTIONS = new Options();
        Option option;

        option = Option.builder("g")
                .longOpt("input-genotype")
                .hasArg(true)
                .type(String.class)
                .desc("Path to genotype data containing samples to score.")
                .argName("path")
                .valueSeparator(' ')
                .build();
        OPTIONS.addOption(option);

        option = Option.builder("i")
                .longOpt("ld-pair-file")
                .hasArg(true)
                .type(String.class)
                .desc("Tab separated file with 2 collumns indicating LD pairs to calculate")
                .argName("path")
                .valueSeparator(' ')
                .build();
        OPTIONS.addOption(option);

        option = Option.builder("t")
                .longOpt("input-type")
                .hasArg(true)
                .required()
                .desc("The input data type.\n"
                        + "* PLINK_BED - plink BED BIM FAM files.\n"
                        + "* VCF - bgziped vcf with tabix index file\n")
                .argName("INPUT_TYPE")
                .valueSeparator(' ')
                .build();
        OPTIONS.addOption(option);

        option = Option.builder("o")
                .longOpt("output-prefix")
                .hasArg(true)
                .type(String.class)
                .desc("Prefix for output.")
                .argName("OUTFILE")
                .valueSeparator(' ')
                .build();
        OPTIONS.addOption(option);

    }


    public static Options getOptions() {
        return OPTIONS;
    }

    public static void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.setWidth(999);
        formatter.printHelp(" ", OPTIONS);
    }

}