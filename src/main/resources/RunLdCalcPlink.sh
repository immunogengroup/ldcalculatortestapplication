#!/bin/bash

GENO=<path to 1kg>
IN=<list of target snps>
OUT=<output prefix>

plink -bfile $GENO \
--r2 \
--ld-snp-list $IN \
--ld-window-kb 1000000 \
--ld-window-r2 0.05 \
--out $OUT
