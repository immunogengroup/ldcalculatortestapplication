# LdCalculatorTestingApplication


Quick application to get the R2 between pairs of variants in a reference dataset.

    java -jar LdCalculatorTestingApplication-1.0-SNAPSHOT-all.jar -g <> -i <> -o <> -t <>
    usage:
         -g,--input-genotype <path>     Path to genotype data containing samples to score.
         -i,--ld-pair-file <path>       Tab separated file with 2 collumns indicating LD pairs to calculate
         -o,--output-prefix <OUTFILE>   Prefix for output.
         -t,--input-type <INPUT_TYPE>   The input data type. If not defined will attempt to automatically select the first matching dataset on the specified path
                                        * PLINK_BED - plink BED BIM FAM files.
                                        * VCF - bgziped vcf with tabix index file

For test files and plink script see src/main/resources.
Executable jar can be found in build/libs